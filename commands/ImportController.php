<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Loan;
use yii\console\Controller;
use app\models\User;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ImportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionRun($type)
    {
        if ($type == ""){
            echo "Please specify type of dump you want to run";
            return ;
        }

        if (!in_array($type, ["users", "loans","all"]) ){
            echo "Please specify correct type of dump you want to run";
            return ;
        }


        switch ($type){
            case "users": {
                $this->dumpUsers();
                break;
            }
            case "loans":{
                $this->dumpLoans();
                break;
            }
            case "all":{
                $this->dumpUsers();
                $this->dumpLoans();
                break;
            }
        }

    }


    private function dumpLoans(){

        $data = $this->getData("loans");

        foreach ($data  as $loan_row){
            $loan = new Loan();
            $loan->id = $loan_row->id;
            $loan->user_id = $loan_row->user_id;
            $loan->amount = $loan_row->amount;
            $loan->interest = $loan_row->interest;
            $loan->duration = $loan_row->duration;
            $loan->start_date = date("Y-m-d", $loan_row->start_date);
            $loan->end_date = date("Y-m-d", $loan_row->end_date);
            $loan->campaign = $loan_row->campaign;
            $loan->status = $loan_row->status;
            $loan->save();
        }
    }

    private function dumpUsers(){
        $data = $this->getData("users");

        foreach ($data as $user_row){
            $user = new User();
            $user->id = $user_row->id;
            $user->first_name = $user_row->first_name;
            $user->last_name = $user_row->last_name;
            $user->email = $user_row->email;
            $user->personal_code = $user_row->personal_code;
            $user->phone = $user_row->phone;
            $user->active = $user_row->active;
            $user->dead = $user_row->dead;
            $user->lang = $user_row->lang;
            $user->save();
        }
    }


    private function getData($type){
        $jsonData = file_get_contents(\Yii::getAlias('@root_dir')."/".$type.".json");
        $decodedData = json_decode($jsonData);
        return $decodedData;
    }

}
