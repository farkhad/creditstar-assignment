<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use conquer\select2\Select2Widget;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\User;


/* @var $this yii\web\View */
/* @var $model app\models\Loan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php

        echo $form->field($model, 'user_id')->widget(
            Select2Widget::className(),
            [
                'ajax' => ['loan/search'],
                'options' => ['placeholder' => 'Select User'],
                'value' => $model->user_id,
                'data' => $model->selectedUser,
            ]
        );

    ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'interest')->textInput() ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'start_date')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
        "inline" => false,
        'options' => ['class' => 'form-control'],
        "containerOptions" => [

        ],
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'end_date')->widget(DatePicker::class, [
        'dateFormat' => 'yyyy-MM-dd',
        "inline" => false,
        'options' => ['class' => 'form-control'],
        "containerOptions" => [

        ],
        'clientOptions' => [
            'changeMonth' => true,
            'changeYear' => true,
        ],
    ]);
    ?>

    <?= $form->field($model, 'campaign')->textInput() ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
