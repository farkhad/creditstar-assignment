<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Loan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'format' => 'raw',
                'attribute' => 'user',
                'value' => function($model){
                    if($model->user){
                        return Html::a(Html::encode($model->user->fullName),['/user/view','id' => $model->user->id]);
                    }
                    return null;
                }
            ],
            'amount',
            'interest',
            'duration',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
