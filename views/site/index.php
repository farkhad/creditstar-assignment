<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\grid\GridView;
use yii\helpers\Html; ?>
<div class="site-index">


    <div class="inner-menu">
        <ul class="list-inline">
            <li>
                <?=Html::a('Users','/user/index');?>
            </li>
            <li>
                <?=Html::a('Loans','/loan/index');?>
            </li>
        </ul>
    </div>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table'],
        'layout' => "{items}",
        'columns' => [
            [
                'format' => 'raw',
                'attribute' => 'user',
                'value' => function($model){
                    if($model->user){
                        return Html::a(Html::encode($model->user->fullName),['/user/view','id' => $model->user->id]);
                    }
                    return null;
                }
            ],
            'amount',
            'start_date',
            'end_date'
        ],
    ]); ?>


</div>
