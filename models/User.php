<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 * @property string $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
            [['active', 'dead'], 'boolean'],
            [['email'], 'email'],
            ['personal_code','match','pattern'=>'/^[0-9]{11}$/' , "message"=>"Must be 11 digits"]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    public function getFullName(){
        return $this->first_name." ".$this->last_name;
    }

    public function getDateOfBirth(){
        $centuryInd = intval(substr($this->personal_code,0,1 ));
        $century = 18;
        $centuriesToAdd = 0;
        if ($centuryInd % 2 == 0){
            $centuryInd--;
        }

        $centuriesToAdd  = $centuriesToAdd + floor($centuryInd/2);
        $century = $century + $centuriesToAdd;
        $century*=100;

        $year = $century + intval(substr($this->personal_code,1,2 ));
        $month = intval(substr($this->personal_code,3,2 ));
        $day = intval(substr($this->personal_code,5,2 ));

        $date = new \DateTime();
        $date->setDate($year, $month, $day);
        return $date;
    }

    public function getAge(){
        $dateOfBirth = $this->getDateOfBirth();
        $today = new \DateTime();
        $diff=date_diff($dateOfBirth ,$today);
        return $diff->y;
    }

}
