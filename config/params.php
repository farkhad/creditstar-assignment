<?php

Yii::setAlias('@root_dir', "/app/");

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'tester@example.com',
    'senderName' => 'tester',
];
