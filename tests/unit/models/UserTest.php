<?php
namespace tests\models;
use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserAndDate()
    {
        expect_that($user = User::findOne(7521));
        expect($user->getDateOfBirth()->format('Y-m-d'))->equals('1990-05-02');
    }
}
